#!/usr/bin/env python
# -*- coding: utf-8 -*-
import discord
import logging
import json
import os
import random
import threading
import time
import traceback
import asyncio
import sys

from classes.datamanager import DataManager
from classes.utils import Utils

logging.basicConfig(level=logging.INFO)

client = discord.Client()
datamanager = DataManager()
utils = Utils()

config = []
messages = []
owner = None
muted_list = {}

def main():
    global config
    global messages
    config = datamanager.get_config()
    messages = datamanager.get_messages()
    if len(sys.argv) == 3:
        client.run(sys.argv[1], sys.argv[2])
    else:
        client.run(config['token'])

# 1 arg = message
# 2 arg = message, member
# 3 args = message, member, answer
def solve_mentions(*args):
    global owner
    if owner is None:
        owner = utils.get_member_from_id(client, config['owner_id'])
    if len(args) == 1:
        return args[0].replace('<ownermention>', owner.mention) \
            .replace('<botmention>', client.user.mention)
    elif len(args) == 2:
        return args[0].replace('<ownermention>', owner.mention) \
            .replace('<botmention>', client.user.mention) \
            .replace('<usermention>', args[1].mention)
    elif len(args) == 3:
        return args[0].replace('<botmention>', client.user.mention) \
            .replace('<usermention>', args[1].mention) \
            .replace('<answer>', args[2])

@client.event
async def on_message(message):
    if client.user in message.mentions and message.author != client.user:
        if utils.check_valid_channel(message.channel, config['excluded_channels']):
            bot_answers = datamanager.get_bot_answers_file()
            await client.send_message(message.channel, message.author.mention + ": " + utils.get_random_from_list(bot_answers))

    if message.content.startswith('!'):
        lowerCaseContent = message.content.lower()
        
        if lowerCaseContent.startswith('!help'):
            mod_roles = datamanager.get_mod_roles()
            member = utils.get_member_from_user(client, message.author)
            is_admin = utils.check_user_is_mod_or_admin(member, mod_roles)
            if is_admin or message.author.id == config['owner_id']:
                admin_help = datamanager.get_admin_help_file()
                await client.send_message(member, solve_mentions(admin_help))
            else:
                user_help = datamanager.get_user_help_file()
                await client.send_message(member, solve_mentions(user_help))

        if lowerCaseContent.startswith('!musichelp'):
            mod_roles = datamanager.get_mod_roles()
            member = utils.get_member_from_user(client, message.author)
            is_admin = utils.check_user_is_mod_or_admin(member, mod_roles)
            if is_admin or message.author.id == config['owner_id']:
                admin_help = datamanager.get_admin_musichelp_file()
                await client.send_message(member, solve_mentions(admin_help))
            else:
                user_help = datamanager.get_user_musichelp_file()
                await client.send_message(member, solve_mentions(user_help))

        if lowerCaseContent.startswith('!report'):
            if message.channel.is_private:
                await client.send_message(message.author, messages['errors']['no_private_channel'])
            else:
                report = message.content.split('!report')
                if len(report[1]) < 5:
                    await client.send_message(message.channel, messages['method_specific']['report_notext'])
                else:
                    await client.send_message(utils.get_channel_from_name(message.author.server, config['reports_channel_name']), solve_mentions(messages['method_specific']['report_format'], message.author, report[1].strip()))
                    await client.send_message(message.channel, messages['method_specific']['report_submit'])

        if lowerCaseContent.startswith('!roll'):
            param = message.content.split('!roll')
            try:
                if param[1].strip() == '':
                    await client.send_message(message.channel, solve_mentions(messages['method_specific']['roll_answer'], message.author, str(random.randint(0,100))))
                else:
                    nume = int(param[1])
                    await client.send_message(message.channel, solve_mentions(messages['method_specific']['roll_answer'], message.author, str(random.randint(0,nume))))
            except ValueError:
                await client.send_message(message.channel, messages['errors']['value_error_exception'])

        if lowerCaseContent.startswith('!avatar'):
            if message.channel.is_private:
                await client.send_message(message.author, messages['errors']['no_private_channel'])
            else:
                members = message.mentions
                if len(members) > 0:
                    for member in members:
                        if not member.avatar_url:
                            await client.send_message(message.channel, messages['errors']['no_avatar'])
                        else:
                            await client.send_message(message.channel, solve_mentions(messages['method_specific']['avatar_answer'], member, member.avatar_url))
                else:
                    await client.send_message(message.channel, messages['errors']['forgot_mention'])

        if lowerCaseContent.startswith('!prune'):
            if message.channel.is_private:
                await client.send_message(message.author, messages['errors']['no_private_channel'])
            else:
                if utils.check_user_manage_messages(message.author) or message.author.id == config['owner_id']:
                    param = message.content.split()
                    try:
                        number = int(param[1])
                        members = message.mentions
                        await client.delete_message(message)
                        if len(members) > 0:
                            await utils.delete_channel_messages_from_members(message.channel, members, number, client)
                        else:
                            await utils.delete_channel_messages(message.channel, number, client)
                    except ValueError:
                        await client.send_message(message.channel, messages['errors']['value_error_exception'])
                else:
                    await client.send_message(message.channel, messages['errors']['no_permission'])

        if lowerCaseContent.startswith('!kick'):
            if message.channel.is_private:
                await client.send_message(message.author, messages['errors']['no_private_channel'])
            else:
                mod_roles = datamanager.get_mod_roles()
                is_admin = utils.check_user_is_mod_or_admin(message.author, mod_roles)
                if is_admin or message.author.id == config['owner_id']:
                    members = message.mentions
                    if len(members) > 3:
                        await client.send_message(message.channel, messages['errors']['too_many_users'])
                    else:
                        for member in members:
                            is_admin = utils.check_user_is_mod_or_admin(member, mod_roles)
                            if not is_admin and not message.id == config['owner_id']:
                                await client.kick(member)
                            else:
                                await client.send_message(message.channel, messages['errors']['delete_kick_or_ban_high_privilege_role'])
                else:
                    await client.send_message(message.channel, messages['errors']['no_permission'])

        if lowerCaseContent.startswith('!ban'):
            if message.channel.is_private:
                await client.send_message(message.author, messages['errors']['no_private_channel'])
            else:
                mod_roles = datamanager.get_mod_roles()
                is_admin = utils.check_user_is_mod_or_admin(message.author, mod_roles)
                if is_admin or message.author.id == config['owner_id']:
                    members = message.mentions
                    if len(members) > 3:
                        await client.send_message(message.channel, messages['errors']['too_many_users'])
                    else:
                        for member in members:
                            is_admin = utils.check_user_is_mod_or_admin(member, mod_roles)
                            if not is_admin and not message.id == config['owner_id']:
                                await client.ban(member)
                            else:
                                await client.send_message(message.channel, messages['errors']['delete_kick_or_ban_high_privilege_role'])
                else:
                    await client.send_message(message.channel, messages['errors']['no_permission'])

        if lowerCaseContent.startswith('!unban'):
            if message.channel.is_private:
                await client.send_message(message.author, messages['errors']['no_private_channel'])
            else:
                mod_roles = datamanager.get_mod_roles()
                is_admin = utils.check_user_is_mod_or_admin(message.author, mod_roles)
                if is_admin or message.author.id == config['owner_id']:
                    members = await client.get_bans(message.server)
                    unban_list = message.content.split()[1:]
                    for member in members:
                        for unban_name in unban_list:
                            if member.name.lower() == unban_name.lower():
                                await client.unban(message.server, member)
                                break
                else:
                    await client.send_message(message.channel, messages['errors']['no_permission'])

        if lowerCaseContent.startswith('!member'):
            if message.channel.is_private:
                await client.send_message(message.author, messages['errors']['no_private_channel'])
            else:
                mod_roles = datamanager.get_mod_roles()
                is_admin = utils.check_user_is_mod_or_admin(message.author, mod_roles)
                if is_admin or message.author.id == config['owner_id']:
                    members = message.mentions
                    for member in members:
                        await client.add_roles(member, utils.search_role('member', message.server.roles))
                else:
                    await client.send_message(message.channel, messages['errors']['no_permission'])

        if lowerCaseContent.startswith('!nsfw'):
            if message.channel.is_private:
                await client.send_message(message.author, messages['errors']['no_private_channel'])
            else:
                mod_roles = datamanager.get_mod_roles()
                is_admin = utils.check_user_is_mod_or_admin(message.author, mod_roles)
                if is_admin or message.author.id == config['owner_id']:
                    members = message.mentions
                    for member in members:
                        await client.add_roles(member, utils.search_role('nsfw', message.server.roles))
                else:
                    await client.send_message(message.channel, messages['errors']['no_permission'])

        if lowerCaseContent.startswith('!colour'):
            if message.channel.is_private:
                await client.send_message(message.author, messages['errors']['no_private_channel'])
            else:
                mod_roles = datamanager.get_mod_roles()
                is_admin = utils.check_user_is_mod_or_admin(message.author, mod_roles)
                if is_admin or message.author.id == config['owner_id']:
                    colour = message.content.split()
                    if len(colour) < 3:
                        await client.send_message(message.channel, messages['errors']['incorrect_number_parameters'])
                    elif len(colour[1].strip()) != 6:
                        await client.send_message(message.channel, messages['method_specific']['invalid_colour'])
                    else:
                        role = utils.search_role('col_'+colour[1].strip(), message.server.roles)
                        if role is None:
                            try:
                                permissions = discord.Permissions()
                                role = await client.create_role(message.server, name='col_'+colour[1].strip().lower(), colour=discord.Colour(int(colour[1].strip(),16)), permissions=permissions)
                            except ValueError:
                                await client.send_message(message.channel, messages['method_specific']['invalid_colour'])
                        if not role.permissions.manage_roles and not role.permissions.manage_messages:
                            members = message.mentions
                            for member in members:
                                act = utils.search_roles_no_colours(member)
                                await client.replace_roles(member, *(act+[role]))
                else:
                    await client.send_message(message.channel, messages['errors']['no_permission'])

        if lowerCaseContent.startswith('!mute'):
            if message.channel.is_private:
                await client.send_message(message.author, messages['errors']['no_private_channel'])
            else:
                mod_roles = datamanager.get_mod_roles()
                is_admin = utils.check_user_is_mod_or_admin(message.author, mod_roles)
                if is_admin or message.author.id == config['owner_id']:
                    members = message.mentions
                    parameters = message.content.split()
                    if len(parameters) < 3:
                        await client.send_message(message.channel, messages['errors']['incorrect_number_parameters'])
                    elif len(members) == 0:
                        await client.send_message(message.channel, messages['errors']['forgot_mention'])
                    else:
                        try:
                            tiempo_total = int(parameters[1])
                            muted = utils.search_role('muted', message.server.roles)
                            for member in members:
                                is_admin = utils.check_user_is_mod_or_admin(member, mod_roles)
                                if not is_admin and not message.id == config['owner_id']:
                                    muted_list[member.id+message.server.id] = asyncio.Future()
                                    await utils.set_role_time_thread(muted, member, tiempo_total, client, muted_list[member.id+message.server.id])
                                else:
                                    await client.send_message(message.channel, messages['errors']['mute_high_privilege_role'])
                        except ValueError:
                            await client.send_message(message.channel, messages['errors']['value_error_exception'])
                else:
                    await client.send_message(message.channel, messages['errors']['no_permission'])


        if lowerCaseContent.startswith('!unmute'):
            if message.channel.is_private:
                await client.send_message(message.author, messages['errors']['no_private_channel'])
            else:
                mod_roles = datamanager.get_mod_roles()
                is_admin = utils.check_user_is_mod_or_admin(message.author, mod_roles)
                if is_admin or message.author.id == config['owner_id']:
                    members = message.mentions
                    if len(members) == 0:
                        await client.send_message(message.channel, messages['errors']['forgot_mention'])
                    else:
                        for member in members:
                            try:
                                muted_list[member.id+message.server.id].set_result('Unmuted!')
                            except KeyError:
                                await client.send_message(message.channel, messages['errors']['not_muted'])

                else:
                    await client.send_message(message.channel, messages['errors']['no_permission'])

        if lowerCaseContent.startswith('!promote'):
            if message.channel.is_private:
                await client.send_message(message.author, messages['errors']['no_private_channel'])
            else:
                if utils.check_user_manage_roles(message.author) or message.author.id == config['owner_id']:
                    role = utils.get_rol_from_message(message.content, message.server.roles)
                    if role is None:
                        await client.send_message(message.channel, messages['errors']['role_doesnt_exist'])
                    else:
                        members = message.mentions
                        for member in members:
                            await client.add_roles(member, role)
                else:
                    await client.send_message(message.channel, messages['errors']['no_permission'])

        if lowerCaseContent.startswith('!demote'):
            if message.channel.is_private:
                await client.send_message(message.author, messages['errors']['no_private_channel'])
            else:
                if utils.check_user_manage_roles(message.author) or message.author.id == config['owner_id']:
                    role = utils.get_rol_from_message(message.content, message.server.roles)
                    if role is None:
                        await client.send_message(message.channel, messages['errors']['role_doesnt_exist'])
                    else:
                        members = message.mentions
                        for member in members:
                            await client.remove_roles(member, role)
                else:
                    await client.send_message(message.channel, messages['errors']['no_permission'])

        if lowerCaseContent.startswith('!faq'):
            if message.channel.is_private:
                await client.send_message(message.author, messages['errors']['no_private_channel'])
            else:
                mod_roles = datamanager.get_mod_roles()
                is_admin = utils.check_user_is_mod_or_admin(message.author, mod_roles)
                if is_admin or message.author.id == config['owner_id']:
                    faq = datamanager.get_faq()
                    param = message.content.split('!faq')[1].strip()
                    if param in faq:
                        await client.send_message(message.channel, faq[param])
                    else:
                        await client.send_message(message.channel, messages['method_specific']['no_entry'])
                else:
                    await client.send_message(message.channel, messages['errors']['no_permission'])

@client.event
async def on_member_join(member):
    await client.send_message(member.server.default_channel,  solve_mentions(messages['greetings']['on_member_join'], member).replace('<announcementmention>', utils.get_channel_from_name(member.server, config['announcements_channel_name']).mention).replace("<servername>", member.server.name))

@client.event
async def on_member_remove(member):
    await client.send_message(member.server.default_channel,  solve_mentions(messages['greetings']['on_member_remove'], member))

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

if __name__ == "__main__":
    while True:
        try:
            main()
        except Exception:
            traceback.print_exc()
        time.sleep(120)
