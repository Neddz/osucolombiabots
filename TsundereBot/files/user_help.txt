**No es como si quisiera ayudar, b-baka.**

*Comandos generales:*
!help - Ayuda
!musichelp - Ayuda con los comandos de música
!roll <num> - Rollear un número de 0 a <num>. Por defecto <num> es 100.
!avatar @user(s) - Muestra una URL para ver el avatar de los usuarios mencionados
!report <message> - Reportar bakas

Contacten a <ownermention> en caso de tener problemas. No es como si yo quisiera hablar contigo de todas maneras.
