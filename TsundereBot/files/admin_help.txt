**No es como si quisiera ayudar, b-baka.**

*Comandos generales:*
!help - Ayuda
!musichelp - Ayuda con los comandos de música
!roll <num> - Rollear un número de 0 a <num>. Por defecto <num> es 100.
!avatar @user(s) - Muestra una URL para ver el avatar de los usuarios mencionados
!report <message> - Reportar bakas
!colour <hexcode> @user(s) - Le pone al (o los) usuarios el color indicado por <hexcode> (requiere permisos)
!mute <time> @user(s) - Mutea al o los usuarios mencionados por <time> segundos (requiere permisos)
!unmute @user(s) - Le quita el mute al o los usuarios mencionados (requiere permisos)
!kick @user(s) - Kickea uno o más usuarios (requiere permisos)
!ban @user(s) - Banea uno o más usuarios (requiere permisos)
!unban user(s) - unbanea uno o más usuarios, se deben poner los nombres ya que no se pueden mencionar (requiere permisos)
!prune # (@user(s)) - Borra los últimos # mensajes del canal donde se use, si se le pasan usuarios por parámetro, elimina únicamente los de esos usuarios en los últimos # mensajes (requiere permisos)
!member @user(s) - Promueve a un usuario a Member (requiere permisos)
!nsfw @user(s) - Le da a un miembro el tag NSFW y le permite el acceso a éste tipo de contenido (requiere permisos)
!promote RoleName @user(s) - Promueve a uno o más usuarios a un Rol específico (requiere permisos)
!demote RoleName @user(s) - Remueve a uno o más usuarios a un Rol específico (requiere permisos)

Contacten a <ownermention> en caso de tener problemas. No es como si yo quisiera hablar contigo de todas maneras.
